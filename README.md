# Group_07
# Project Agros

# The project

This module provides a class for analyzing and visualizing agricultural data.
It reads a dataset of agricultural data by country and year and filters for only 1970 and later.
It provides methods for information about the data and creating visualizations.
It also downloads and merges with geospatial data to create spatial visualisations.

Link to the Agricultural dataset:
https://raw.githubusercontent.com/owid/owid-datasets/master/datasets/Agricultural%20total%20factor%20productivity%20(USDA)/Agricultural%20total%20factor%20productivity%20(USDA).csv

# The group

This project was developed by Miguel Rocha (53654), Miguel Almeida (53577), Victor Høgheim (53665) and Alex Theilmann (53679), respectively, two Portuguese, a Norwegian and a German.

We are currently doing a Master's in Business Analytics at NOVA SBE. All in our early 20s, we bring diverse backgrounds to this project, from Mathematics to Computer Science and Business.

We hope you find it as useful and interesting as we aspire it to be and that all insights and content are equally as clear and accessible.

Contact:
Miguel Rocha 53654@novasbe.pt
Miguel Almeida 53577@novasbe.pt
Victor Høgheim 53665@novasbe.pt
Alex Theilmann 53679@novasbe.pt

# Dependencies

- pandas
- seaborn
- matplotlib
- geopandas
- pmdarima

# Installation

To use this project, clone the repository to your local machine:

`git clone git@gitlab.com:victordh/group_07.git`

Then, install the required dependencies (first option):

`pip install pandas`

`pip install seaborn`

`pip install matplotlib`

`pip install geopandas`

`pip install pmdarima`

Another option (yml file):

`Open your Anaconda prompt`

`Go to the directory group_07`

`Run this on your terminal "conda env create -f environment.yml"`

# Usage

_# Import module and initialize object_

`from files.methods import Agro`

`agro = Agro()`

_# Get list of unique countries in the dataset_

`agro.countries()`

_# Create a heatmap of the correlation between_ "_quantity" columns_

`agro.quantity_corr()`

_# Plot an area chart of the distinct "_output_" columns for a country_

`agro.output_area_chart(country = "Portugal", normalize = True)`

_# Plot a comparison of the total of the "_output_" columns for each of the chosen countries_

`agro.total_output_line_chart(country = ["Norway", "Canada"])`

_# Plot a gapminder of fertilizer_quantity, output_quantity, and labor_quantity within a given year_

`agro.gapminder(year = 2019, log = True)`

_# Plot a choropleth of total factor productivity within a given year_

`choropleth_tfp(year = 2019)`

_# Plot a prediction of total factor productivity for specified countries until 2050_

`predictor_tfp(countries = ['Portugal', 'Norway', 'Germany'])`

# Support
If you have any questions or issues, please contact @victordh on GitLab or send a mail to one of the group members.

# Roadmap
Future updates to the project may include:

- Additional data sources
- Machine learning models for predicting crop yields

# Contributing

Contributions to the project are welcome. Please reach out to one of the authors.

# Authors and acknowledgment

This project was completed by _Miguel Rocha_, _Miguel Almeida_, _Victor Høgheim_ and _Alex Theilmann_ as part of _Advanced Programming for Data Science_ at _Nova School of Business and Economics_. Special thanks to _Luís Guimarãis_ for his guidance and support.

# License
This project is licensed under the _GNU GENERAL PUBLIC LICENSE Version 3_. See the LICENSE file for more details.

# Project status
This project is currently paused.
