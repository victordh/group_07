'''
Agro - A module providing a class for analyzing and visualizing agricultural data.

It reads a dataset of agricultural data by country and year and filters for only 1970 and later.
It provides methods for information about the data and creating visualizations.
It also downloads and merges with geospatial data to create spatial visualization.

Example usage:

    from files.methods import Agro

    - agro = Agro()
    - agro.countries()
    - agro.quantity_corr()
    - agro.output_area_chart(country = 'Portugal', normalize = True)
    - agro.total_output_line_chart(country = ['Norway', 'Canada'])
    - agro.gapminder(year = 2019, log = True)
    - agro.choropleth_tfp(year = 2019)
    - agro.predictor_tfp(countries = ['Norway', 'Canada', 'Germany'])

Dependencies:
    - pmdarima
    - pandas
    - geopandas
    - seaborn
    - matplotlib
'''

import os
import warnings
from typing import Union, List
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import geopandas as gpd
from pmdarima import auto_arima

warnings.filterwarnings("ignore")

class Agro:
    '''
    A class for analyzing and visualizing agricultural data.

    This class reads agricultural and geographical data and filters for only years 1970 and later.
    If the data files is not already downloaded, creates downloads folder and stores it.
    It provides methods for accessing the data and creating visualizations using the seaborn theme.

    Variables:
        merge_dict (dict): Containing a dictionary with country names to change in geo_data

    Attributes:
        agro_data (pandas.DataFrame): Containing the agricultural data.
        geo_data (geopandas.DataFrame): Containing the geographical data.

    Methods:
        countries(): Returns a list of unique countries in the dataset.
        quantity_corr(): Creates a heatmap of the correlation between _quantity columns.
        output_area_chart(country = None, normalize = False): Plots an area chart of the distinct
                                                              '_output_' columns.
        total_output_line_chart(country: Union[str, List[str]]): Plots a comparison of the total
                                                                 of the 'output' columns for each
                                                                 of the chosen countries.
        gapminder(year: int, log = True): Plots a gapminder-like representation of
                                          fertilizer_quantity, output_quantity and
                                          labor_quantity within a given year.
        choropleth(year: int): Merges agro_data with geo_data to plot a choropleth of the
                               tfp column within a given year.
        predictor(countries: List[str]): Predicts TFP values for the specified countries and
                                         displays the actual data and predictions in a plot.
    '''

    # class variable (dict) with country names to change in geo data
    merge_dict: dict = {'United States of America': 'United States',
                        'Dem. Rep. Congo': 'Democratic Republic of Congo',
                        'Dominican Rep.': 'Dominican Republic',
                        'Timor-Leste': 'Timor',
                        'Central African Rep.': 'Central African Republic',
                        'Eq. Guinea': 'Equatorial Guinea',
                        'eSwatini': 'Eswatini',
                        'Solomon Is.': 'Solomon Islands',
                        'Bosnia and Herz.': 'Bosnia and Herzegovina',
                        'S. Sudan': 'South Sudan'}

    def __init__(self) -> None:
        # Check if download folder exists, create if not
        if not os.path.exists('downloads'):
            os.mkdir('downloads')

        # Try to download from local folder, if not there, read from URL and save
        try:
            agro_data: pd.DataFrame = pd.read_csv('downloads/agro-data.csv')
        except FileNotFoundError:
            url: str = ('https://raw.githubusercontent.com/owid/owid-datasets/master/datasets/'
                        'Agricultural%20total%20factor%20productivity%20(USDA)/'
                        'Agricultural%20total%20factor%20productivity%20(USDA).csv')
            agro_data: pd.DataFrame = pd.read_csv(url)
            agro_data.to_csv('downloads/agro-data.csv', index=False)

        # Download and save geographical data if not already saved locally
        if not os.path.exists('downloads/geo-data.geojson'):
            geo_data: gpd.GeoDataFrame = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
            geo_data.to_file('downloads/geo-data.geojson', driver='GeoJSON')
        else:
            geo_data: gpd.GeoDataFrame = gpd.read_file('downloads/geo-data.geojson')

        # Remove entities that are not coutries and filter for only 1970 and later
        not_countries = ['Asia', 'Caribbean', 'Central Africa', 'Central America',
                         'Central Asia', 'Central Europe', 'Developed Asia',
                         'Developed countries', 'Czechoslovakia', 'East Africa',
                         'Eastern Europe', 'Europe', 'Former Soviet Union', 'High income',
                         'Latin America and the Caribbean', 'Least developed countries',
                         'Low income', 'Lower-middle income', 'North Africa', 'North America',
                         'Northeast Asia', 'Northern Europe', 'Serbia and Montenegro',
                         'South Asia', 'Southeast Asia', 'Southern Africa', 'Southern Europe',
                         'Upper-middle income', 'West Africa', 'West Asia', 'Western Europe',
                         'World', 'Pacific', 'Oceania', 'Horn of Africa', 'Yugoslavia',
                         'Sub-Saharan Africa']

        self.agro_data = agro_data[(agro_data.Year >= 1970) &
                                   ~(agro_data.Entity.isin(not_countries))]
        self.geo_data = geo_data

        # seaborn theme used for visualization
        sns.set()

    def countries(self) -> List[str]:
        '''
        Returns a list of unique countries in the dataset.

        Returns:
            A list of strings representing the unique countries in the dataset.
        '''
        return self.agro_data.Entity.unique().tolist()

    def quantity_corr(self) -> None:
        '''
        Creates a heatmap of the correlation between _quantity columns.

        Returns:
            None
        '''
        # Calculate correlation of all columns ending in _quantity
        corr = self.agro_data.loc[:, self.agro_data.columns.str.endswith('_quantity')].corr()

        # Visualise with a heatmap
        fig, ax = plt.subplots(figsize=(10, 5))
        sns.heatmap(corr, cmap='YlGnBu', annot=True, annot_kws={'size': 10})

        ax.set_xticklabels(ax.get_xticklabels(), rotation=60, fontsize=10)
        ax.set_yticklabels(ax.get_yticklabels(), rotation=0, fontsize=10)

        plt.title('Correlation of Quantity Columns')
        # Add annotation, specify font size to fit better with labels
        plt.text(0.05, -0.25, '''Source: Agricultural total factor productivity (USDA),
                 Our World in Data (2020)''', transform=plt.gcf().transFigure,
                 ha='left', fontsize=10)
        plt.show()

    def output_area_chart(self, country: str = None, normalize: bool = False) -> None:
        '''
        Plots an area chart of the distinct '_output_' columns.

        Args:
            country (str): A string representing the name of the country.
                           If None or 'World', plots the sum for all distinct countries.
            normalize (bool): A boolean indicating whether the output
                              should be normalized in relative terms.

        Returns:
            None

        Raises:
            TypeError: If the country parameter is not a string.
            TypeError: If the normalize parameter is not a boolean.
            ValueError: If the chosen country does not exist in the dataframe.
        '''
        # Check the input types
        if not isinstance(country, str) and country is not None:
            raise TypeError('The "country" parameter must be a string or None.')

        if not isinstance(normalize, bool):
            raise TypeError('The "normalize" parameter must be a boolean.')

        # Filter the dataframe based on the country parameter
        if country is None or country.lower() == 'world':
            data = self.agro_data
        else:
            data = self.agro_data[self.agro_data.Entity == country]
            # Check if country exists in data
            if len(data) == 0:
                raise ValueError('The input "country" does not exist in the dataframe.')

        # Group by year and select only the output columns
        output_sum = data.groupby('Year').sum(numeric_only=True)
        output_sum = output_sum.loc[:, output_sum.columns.str.contains('_output_')]

        # Normalize the output if specified
        if normalize:
            output_sum = output_sum.divide(output_sum.sum(axis=1), axis=0) * 100

        # Create the area chart
        fig, ax = plt.subplots(figsize=(10, 5))

        ax.stackplot(output_sum.index, output_sum.values.T, labels=output_sum.columns)
        ax.legend(['Crop', 'Animal', 'Fish'])

        ax.set_xlabel('Year')
        ax.set_ylabel('Output Quantity')

        plt.title('Output Area Chart')
        plt.text(0.05, -0.1, '''Source: Agricultural total factor productivity (USDA),
                 Our World in Data (2020)''', transform=plt.gcf().transFigure, ha='left')
        plt.show()

    def total_output_line_chart(self, country: Union[str, List[str]]) -> None:
        '''
        Plots a comparison of the total of the 'output' columns for each of the chosen countries.

        Args:
            country (Union[str, List[str]]): A string or a list of strings
                                             representing the name(s) of the countries.

        Returns:
            None

        Raises:
            TypeError: If the country is not a string or a list of strings.
            ValueError: If any of the given countries does not exist in the dataframe.
        '''
        # Convert the given country parameter to a list if it is a string, and check for wrong types
        if isinstance(country, str):
            country = [country]
        elif not isinstance(country, list) or not all(isinstance(c, str) for c in country):
            raise TypeError('The "country" parameter must be a string or list of strings.')

        # Check if all of the given countries exist in the dataframe
        if not set(country).issubset(set(self.agro_data.Entity.unique())):
            raise ValueError('At least one of the given countries does not exist in the data.')

        # Define the columns for the output quantities
        output_cols = ['crop_output_quantity', 'animal_output_quantity',
                       'fish_output_quantity']

        # Filter the dataframe based on the given country parameter
        data = self.agro_data[self.agro_data.Entity.isin(country)]

        # Calculate the total output by year and countries
        output_sum = data.groupby(['Year', 'Entity'])[output_cols].sum().reset_index()

        # Create the line chart, one line per country
        fig, ax = plt.subplots(figsize=(10, 5))

        for choice in country:
            output_sum_c = output_sum[output_sum.Entity == choice]
            ax.plot(output_sum_c.Year, output_sum_c[output_cols].sum(axis=1), label=choice)

        ax.legend(title='Country')
        ax.set_xlabel('Year')
        ax.set_ylabel('Output Quantity')

        plt.title('Total Output Quantity Comparison')
        plt.text(0.05, -0.1, '''Source: Agricultural total factor productivity (USDA),
                 Our World in Data (2020)''', transform=plt.gcf().transFigure, ha='left')
        plt.show()

    def gapminder(self, year: int, log: bool = True) -> None:
        '''
        Plots a gapminder plot of fertilizer_quantity, output_quantity and
        labor_quantity within a given year.

        Args:
            year (int): An int representing the year.
            log (bool): A boolean deciding if log scale is used or not

        Returns:
            None

        Raises:
            TypeError: If the year is not an integer.
            TypeError: If the log parameter is not a boolean
        '''
        # Check for wrong input types and raise error
        if not isinstance(year, int):
            raise TypeError('The "year" parameter must be an int.')
        if not isinstance(log, bool):
            raise TypeError('The "log" parameter must be a boolean.')

        # Filter for specified year
        data = self.agro_data[self.agro_data.Year == year]

        # Create gapminder plot
        fig, ax = plt.subplots(figsize=(10, 5))
        sns.scatterplot(x=data.fertilizer_quantity, y=data.output_quantity,
                        size=data.labor_quantity, sizes=(40, 400))

        ax.legend(title='Labor Quantity')
        ax.set_xlabel('Fertilizer Quantity')
        ax.set_ylabel('Output Quantity')

        # If log is True, add log scale
        if log:
            plt.xscale('log')
            plt.yscale('log')

        plt.title('Gapminder Plot')
        plt.text(0.05, -0.1, '''Source: Agricultural total factor productivity (USDA),
                 Our World in Data (2020)''', transform=plt.gcf().transFigure, ha='left')
        plt.show()

    def choropleth_tfp(self, year: int) -> None:
        '''
        Merges agro_data with geo_data to plot a choropleth of the tfp column
        within a given year.

        Args:
            year (int): An int representing the year.

        Returns:
            None

        Raises:
            TypeError: If the year parameter is not an int.
            ValueError: If the year is not in the data.
        '''

        # check for wrong input type and raise error
        if not isinstance(year, int):
            raise TypeError('The "year" parameter must be an int.')

        # change country names
        self.geo_data['name'] = self.geo_data.name.map(lambda x: self.merge_dict[x] if x in
                                                       self.merge_dict.keys() else x)

        # merge agro_data and geo_data
        all_data = pd.merge(self.geo_data, self.agro_data,
                            left_on='name', right_on='Entity')

        # filter for input year, raise if year is not in dataset
        data = all_data[all_data.Year == year]
        if len(data) == 0:
            raise ValueError('The input "year" does not exist in the dataframe.')

        # plot choropleth map
        data.plot(column='tfp', figsize=(10, 5), legend=True, cmap='YlGnBu')

        plt.title('Choropleth Map of Total Factor Productivity')
        plt.xlabel('Longitude')
        plt.ylabel('Latitude')
        plt.annotate('''
                    Source: Agricultural total factor productivity (USDA), Our World in Data (2020)
                    and naturalearth_lowres, Natural Earth (2023)
                    ''', (20, 10), xycoords='figure points')

    def predictor_tfp(self, countries: List[str]) -> None:
        '''
        Predicts TFP values for the specified countries and displays the actual data and
        predictions in a plot.

        Args:
            countries (List[str]): List of country names to predict TFP values for.

        Returns:
            None

        Raises:
            ValueError: If more than 3 countries are entered or if none of the specified
                        countries are present in the Agricultural dataset.
        '''
        if not isinstance(countries, list) or not all(isinstance(c, str) for c in countries):
            raise TypeError('The "country" parameter must be a list of strings.')

        # check if no more than 3 countries were entered
        if len(countries) > 3:
            raise ValueError('No more than 3 countries are allowed.')

        # change countries to lower case in case user writes in lower case
        countries = [x.lower() for x in countries]

        # filter the Agricultural dataset to include only the specified countries
        data = self.agro_data.loc[self.agro_data.Entity.str.lower().isin(countries)]

        # check if any specified country is not present in the dataset
        missing = set(countries) - set(data.Entity.str.lower())

        # only proceed with correct countries
        countries = set(data.Entity)

        if len(missing) == 3:
            raise ValueError(f'''None of the requested countries ({', '.join(countries)}) are
                                 present in the agricultural dataset. The following countries are
                                 available: {self.agro_data.Entity.unique().tolist()}''')

        if len(missing) > 0:
            print('The following countries are not present in the Agricultural dataset and will',
                  f'be ignored: {", ".join(missing)}')

        plt.figure(figsize=(10, 5))

        # define labels
        labels = []

        # define line colors
        colors = ['red', 'blue', 'green']
        index = 0

        # Loop through countries to plot data and prediction for each one
        for country in countries:
            # filter data for each selected country and set index with datetime format
            tfp = data[data.Entity == country][['tfp']]
            tfp.set_index(pd.to_datetime(data.Year.unique(), format='%Y'), inplace=True)

            # Find best hyperparameters
            model = auto_arima(tfp, start_p=1, max_p=5, start_q=1, max_q=3, seasonal=False)

            # Predict next 31 years (to 2050)
            pred = pd.DataFrame(model.predict(n_periods=31))
            pred = pred.set_index(pd.date_range(start='2020-01-01', periods=31, freq='AS-JAN'))

            # plot historic data and prediction for each country
            tfp.plot(ax=plt.gca(), linestyle='-', color=colors[index])
            pred.plot(ax=plt.gca(), linestyle='--', color=colors[index])

            # save legend
            labels.extend([f'{country} data', f'{country} predicted'])

            # counter to set unique color per country
            index += 1

        plt.title('Time Series of Total Factor Productivity')
        plt.legend(labels)
        plt.xlabel('Year')
        plt.ylabel('Total Factor Productivity')
        plt.text(0.05, -0.1, '''Source: Agricultural total factor productivity (USDA),
                 Our World in Data (2020)''', transform=plt.gcf().transFigure, ha='left')
        plt.show()
